#include <stdio.h>
#include <stdlib.h>

#define false 0
#define true 1
#define Player struct PlayerRec

struct PlayerRec
{
	char name[50];
	char teamName[50];
	int* games;
	int totalGoals;
	double averageGoals;
	char rating;
};



void printMenu();
char determineRating(int value);
void cleanUp(Player* players,int numPlayers);

int main(void)
{
	int numGames = 0;
	int numPlayers = 0;
	int running = true;
	double averageGoals;
	Player* players;
	while (running)
	{
		printMenu();
		char menuOption;
		scanf("%c",&menuOption);
		while(menuOption == '\n') scanf("%c",&menuOption);
		switch (menuOption)
		{
			case 'a':
			case 'A':
			{
				printf("Please enter the number of players:\n");
				scanf("%d",&numPlayers);
				printf("Please enter the number of games:\n");
				scanf("%d",&numGames);
				players = malloc(numPlayers * sizeof(Player));
				for (int i = 0; i < numPlayers;i++)
				{
					players[i].games = malloc(numGames * sizeof(int));
					for (int k = 0; k < numGames; k++)
					{
						players[i].games[k] = 0;
					}
				}
				printf("Arrays Initialized\n");
				break;
			}
			case 'b':
			case 'B':
			{
				for (int i  = 0; i < numPlayers;i++)
				{
					printf("Please enter name for Player %d\n",i + 1);
					scanf("%s",players[i].name);
					printf("Please enter %s's team name\n",players[i].name);
					scanf("%s",players[i].teamName);
					for (int k = 0; k < numGames; k++)
					{
						printf("Please enter how many goals %s scored in Game %d\n",players[i].name,k + 1);
						scanf("%d",&players[i].games[k]);
					}
				}

				break;
			}
			case 'c':
			case 'C':
			{
				for (int i = 0; i < numPlayers;i++)
				{
					int total = 0;
					for (int k = 0; k < numGames;k++)
						total += players[i].games[k];
					players[i].totalGoals = total;
					printf("%s scored a total of %d goals\n",players[i].name,total);
				}
				break;
			}
			case 'd':
			case 'D':
			{
				for (int i = 0; i < numPlayers;i++)
				{
					players[i].averageGoals = (double)players[i].totalGoals / numGames;
					printf("%s scored an average of %f goals\n",players[i].name,players[i].averageGoals);
				}
				break;
			}
			case 'e':
			case 'E':
			{
				double sumAverages = 0.0;
				for (int i = 0; i < numPlayers;i++)
				{
					sumAverages += players[i].averageGoals;
				}
				averageGoals = sumAverages / numPlayers;
				printf("The average number of goals scored is %f\n",averageGoals);
				break;
			}
			case 'f':
			case 'F':
			{
				printf("Flagged Players:\n");
				for (int i = 0; i < numPlayers;i++)
				{
					if (players[i].averageGoals >= averageGoals)
						printf("%s\n",players[i].name);
				}

				break;
			}
			case 'g':
			case 'G':
			{
				for (int i = 0; i < numPlayers;i++)
				{
					players[i].rating = determineRating(players[i].totalGoals);
					printf("%s : %c\n",players[i].name,players[i].rating);
				}
				break;
			}
			case 'h':
			case 'H':
			{
				if (numPlayers > 0)
				{
					double highest = players[0].averageGoals;
					int index = 0;
					for (int i = 1; i < numPlayers;i++)
					{
						if (players[i].averageGoals > highest)
						{
							highest = players[i].averageGoals;
							index = i;
						}
					}
					printf("The player with the highest average is %s with an average of %f\n",players[index].name,highest);
				}
				else
				{
					printf("No players have been added\n");
				}
				break;
			}
			case 'x':
			case 'X':
			{
				running = false;
				break;
			}
			default:
			{
				printf("Invalid menu option \"%c\"", menuOption);
			}
		} 
	}
	
	return 0;
}

void printMenu()
{
	printf("Menu:\n");
	printf("A: Initialize\n");
	printf("B: Input Player Info\n");
	printf("C: Calculate and Display Totals\n");
	printf("D: Calculate and Display Averages\n");
	printf("E: Calculate and Display Total Average\n");
	printf("F: Display Flagged Players\n");
	printf("G: Calculate and Display Ratings\n");
	printf("H: Display Best Player\n");
	printf("X: Exit\n");
}

char determineRating(int value)
{
	char res = 0;
	if (value >= 0 && value <= 5) res = 'D';
	if (value >= 6 && value <= 10) res = 'C';
	if (value >= 11 && value <= 15) res = 'B';
	if (value >= 16) res = 'A';
	return res;
}

void cleanUp(Player* players,int numPlayers)
{
	for (int i = 0; i < numPlayers;i++)
	{
		free(players[i].games);
	}
	free(players);
}
